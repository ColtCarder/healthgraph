<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;
$client_id = "get from healthgraph app page";
$client_secret = "get from healthgraph app page";
$auth_url = "https://runkeeper.com/apps/authorize";
$token_url = "https://runkeeper.com/apps/token";
$redirect_uri = "URL to this script";

if(!isset($_REQUEST["code"]))
{
  header('Location: '.$auth_url.'?client_id='.$client_id.'&response_type=code&redirect_uri='.$redirect_uri);
}
else
{
    $client = new Client();
    $response = $client->post($token_url, [
    'body' => [
        'grant_type' => 'authorization_code',
        'code' => $_REQUEST['code'],
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'redirect_uri' => $redirect_uri
    ]
]);
    $json = $response->json();
    echo "Your Auth Token is: ".$json['access_token'];
    
}

